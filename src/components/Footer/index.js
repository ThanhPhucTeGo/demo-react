import "./style.css"
const Footer = () => {
    const date = new Date()
    const getFullYear = date.getFullYear()
    return (
        <footer className="footer">
            <p>
                All &copy; copy rights are reserved to DiviceTech-Shop {getFullYear}
            </p>
        </footer>
    )
}
export default Footer